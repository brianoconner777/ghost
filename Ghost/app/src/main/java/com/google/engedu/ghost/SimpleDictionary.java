package com.google.engedu.ghost;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

public class SimpleDictionary implements GhostDictionary {
    private ArrayList<String> words;

    public SimpleDictionary(InputStream wordListStream) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(wordListStream));
        words = new ArrayList<>();
        String line = null;
        while((line = in.readLine()) != null) {
            String word = line.trim();
            if (word.length() >= MIN_WORD_LENGTH)
              words.add(line.trim());
        }
    }

    @Override
    public boolean isWord(String word) {
        return words.contains(word);
    }

    @Override
    public String getAnyWordStartingWith(String prefix) {
        String currWord = null;
        if (prefix.equals("")){
            currWord = words.get(new Random().nextInt(10000)).substring(0, 4);
        }
        else{
            int start = 0 , mid , last = words.size();
            while(start <= last){
                mid = (start + last)/2;
                if(words.get(mid).startsWith(prefix)){
                    currWord = words.get(mid);
                }
                else if(prefix.compareToIgnoreCase(words.get(mid))<0){
                    last = mid - 1 ;
                }
                else if(prefix.compareToIgnoreCase(words.get(mid))>0){
                    last = mid + 1;
                }
                else if (prefix.compareToIgnoreCase(words.get(mid))==0){
                    currWord = words.get(mid);
                    break;
                }
            }
        }
        return currWord;

    }

    @Override
    public String getGoodWordStartingWith(String prefix) {
        return null;
    }
}
